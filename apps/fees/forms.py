from django.forms import ModelForm

from .models import TransactionFee


class TransactionFeeForm(ModelForm):
	class Meta:
		model = TransactionFee
		fields = ['fee_type', 'rate_type']

