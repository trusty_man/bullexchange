import os
import hashlib
import hmac
import json
import requests
import subprocess
import datetime


from bitcoin import *
from decimal import Decimal
from django.db.models import Sum
from django.shortcuts import get_object_or_404
from pywallet import wallet
from bitcoinrpc.authproxy import AuthServiceProxy

from apps.bitcoin_crypto.models import WalletAddress, Wallet, Transaction, VaultWallet,\
 VaultTransaction, WatchOnlyAddress, OrderBook, SGDWallet, OrderMatchingHistory
from django.db.models import Sum
from apps.fees.models import TransactionFee


CHANGELLY_API_URL = 'https://api.changelly.com'

CHANGELLY_API_KEY = os.environ.get('TWILIO_ACCOUNT_SID')
CHANGELLY_API_SECRET = os.environ.get('TWILIO_ACCOUNT_SID')
BLOCKCIPHER_API_KEY = os.environ.get('TWILIO_ACCOUNT_SID')


def changelly_transaction(method, params):
    message = {
                  "jsonrpc": "2.0",
                  "method": method,
                  "params": params,
                  "id": 1
                }

    serialized_data = json.dumps(message)

    sign = hmac.new(CHANGELLY_API_SECRET.encode('utf-8'), serialized_data.encode('utf-8'), hashlib.sha512).hexdigest()

    headers = {'api-key': CHANGELLY_API_KEY, 'sign': sign, 'Content-type': 'application/json'}
    response = requests.post(API_URL, headers=headers, data=serialized_data)

    return response.json()

def create_connection():
    access = AuthServiceProxy("http://bitcoin:topsun@128.199.212.25:18332")
    return access

def create_btc_wallet(user):
    access = create_connection()
    addr = access.getnewaddress(user.username)
    wallet, created = Wallet.objects.get_or_create(user=user, name='btc')
    wallet.addresses.add(WalletAddress.objects.create(address=addr))
    return addr


def get_btc_balance(user):
    access = create_connection()
    balance = Decimal(access.getreceivedbyaccount(user.username))
    transaction = Transaction.objects.filter(user=user, currency="btc", transaction_type='withdrawal', invalid=False, pending=False, rejected=False)

    if transaction:
        txn_fee = sum([ txn.transaction_fee_except_mining_fees() for txn in transaction]) + Decimal(transaction.aggregate(Sum('mining_fee'))['mining_fee__sum'])
        balance = balance - (sum([Decimal(obj.amount) for obj in transaction]) + txn_fee)

    sell_orders = OrderBook.objects.filter(user=user, order_type=1, canceled=False)
    cancelled_sell_orders = OrderBook.objects.filter(user=user, order_type=1, canceled=True)

    if sell_orders.exists():
        balance = balance - Decimal(sell_orders.aggregate(Sum('amount'))['amount__sum'])

    if cancelled_sell_orders.exists():
          balance = balance - Decimal(sell_orders.aggregate(Sum('coins_covered'))['coins_covered__sum'])

    buy_orders = OrderBook.objects.filter(user=user, order_type=0)
    if buy_orders.exists():
        balance = balance + Decimal(buy_orders.aggregate(Sum('coins_covered'))['coins_covered__sum'])

    return balance


def get_balance(user, currency):
    access = create_connection()
    balance = access.getreceivedbyaccount(user.username)
    transaction = Transaction.objects.filter(user__username=user.username,currency=currency, transaction_type='withdrawal', invalid=False, pending=False, rejected=False)

    if transaction:
        txn_fee = sum([ txn.transaction_fee_except_mining_fees() for txn in transaction]) + Decimal(transaction.aggregate(Sum('mining_fee'))['mining_fee__sum'])
        balance = balance - (sum([Decimal(obj.amount) for obj in transaction]) + txn_fee)

    sell_orders = OrderBook.objects.filter(user=user, order_type=1, canceled=False)
    cancelled_sell_orders = OrderBook.objects.filter(user=user, order_type=1, canceled=True)

    if sell_orders.exists():
        balance = balance - Decimal(sell_orders.aggregate(Sum('amount'))['amount__sum'])

    if cancelled_sell_orders.exists():
          balance = balance - Decimal(sell_orders.aggregate(Sum('coins_covered'))['coins_covered__sum'])

    buy_orders = OrderBook.objects.filter(user=user, order_type=0)
    if buy_orders.exists():
        balance = balance + Decimal(buy_orders.aggregate(Sum('coins_covered'))['coins_covered__sum'])

    return round(balance,8)

def get_fee_balance():
    access = create_connection()
    balance = access.getreceivedbyaccount('transaction fee collector')
    transaction = Transaction.objects.filter(transaction_type="fee_withdrawal", invalid=False, pending=False, rejected=False)

    if transaction:
        txn_fee = Decimal(transaction.aggregate(Sum('mining_fee'))['mining_fee__sum'])
        balance = balance - (sum([Decimal(obj.amount) for obj in transaction]) + txn_fee)

    return round(balance,8) 


def get_vault_balance(username, currency):
    access = create_connection()
    balance = access.getreceivedbyaccount(username)
    transaction = VaultTransaction.objects.filter(user=username, currency=currency)
    if transaction:
        balance = balance - sum([Decimal(obj.amount) for obj in transaction])

    return balance


def get_account_balance():
    access = create_connection()
    balance = access.getbalance()

    return balance


def create_vault_wallet(user, username, currency):
    access = create_connection()
    addr = access.getnewaddress(username)
    wallet, created = VaultWallet.objects.get_or_create(user=user, username=username, name=currency)
    wallet.addresses.add(WalletAddress.objects.create(address=addr))
    return addr

def total_pending_transaction_amount():
    transactions = Transaction.objects.filter(pending=True)
    
    amount = Decimal('0')
    for transaction in transactions:
        amount += Decimal(transaction.amount)

    return amount


def complete_pending_transaction(transaction):
    access = create_connection()
    balance = access.getbalance()

    try:
        amount = Decimal(transaction.amount)
    except:
        amount = Decimal(0)
    
    transaction_fee = Decimal(transaction.transaction_fee)

    # getaccountaddress will be depricated in later version of bitcoin
    transaction_fee_to = access.getaccountaddress('transaction fee collector')

    if balance >= (amount+transaction_fee):
        if transaction_fee > Decimal(0):
            valid = access.sendmany(transaction.user.username,{transaction.transaction_to: amount, transaction_fee_to : transaction_fee}, 
            6,"",[transaction_fee_to])
        else:
            valid = access.sendtoaddress(transaction.transaction_to, amount)

        if valid:
            transaction.pending = False
            transaction.transaction_id = valid
            rpc_tansaction = access.gettransaction(valid)
            mining_fee = rpc_tansaction['fee']
            transaction.mining_fee = str(abs(mining_fee))
            transaction.date = datetime.datetime.fromtimestamp(float(rpc_tansaction['time']))
            transaction.save()

            return True
        else:
            transaction.invalid = True
            transaction.save()
            return 'Invalid Transaction'
            
    return 'Please Maintain Main Wallet Amount to Complete this Transaction.'


def recived_by_address(address):
    access = create_connection()
    amount = access.getreceivedbyaddress(address)
    if amount == Decimal('0E-8'):
        return Decimal('0.00')
    return amount


def checkwatchonlyaddress():
    access = create_connection()
    vault_list = WatchOnlyAddress.objects.all()
    for obj in vault_list:
        if not access.validateaddress(obj.address)['iswatchonly']:
            obj.delete()


def get_publict_key(vault_address):
    access = create_connection()
    vault_list = WatchOnlyAddress.objects.all()
    for obj in vault_list:
        try:
            prv_key = access.dumpprivkey(obj.address)
            if prv_key == vault_address:
                return obj.address
        except:
            pass

    return 'non vault address'


def get_transaction_fee(amount, currency, fee_type):
    """return transction fee of parameters amount, currecny, fee type"""

    # checking any transaction fee object exist else return fees to zero

    try:
        transaction_fee_obj = get_object_or_404(TransactionFee, currency=currency, fee_type=fee_type)
    except:
        return Decimal(0)

    fee_limits = transaction_fee_obj.transactionfeerange_set.all()

    #identifing fee range object of amount
    fee_obj = None
    for fee_limit in fee_limits:
        if Decimal(fee_limit.value) <= Decimal(amount):
            if fee_obj:
                fee_obj = fee_limit if Decimal(fee_obj.value) <  Decimal(fee_limit.value) else fee_obj
            else:
                fee_obj = fee_limit

    #calculating transaction fees according to rate type
    if not fee_obj:
        return Decimal(0)
        
    if transaction_fee_obj.rate_type == 'percentage':
        fee = Decimal(amount) * Decimal(fee_obj.fees)/100
    else:
        fee =  Decimal(fee_obj.fees)

    return fee


def complete_order(request, order_instance):
    
    if order_instance.order_type == '0':

        #processing buy order
        if order_instance.order_mode == '0':
            # market order
            sell_orders = OrderBook.objects.filter(order_type='1', trade_status=False).order_by('price','id')
        elif order_instance.order_mode == '1':
            #limit order
            sell_orders = OrderBook.objects.filter(order_type='1', trade_status=False, price__lte=order_instance.price).order_by('price','id')
        elif order_instance.order_mode == '2':
            #stop-limit order
            limit = order_instance.limit
            sell_orders = OrderBook.objects.filter(order_type='1', trade_status=False, price__lte=order_instance.price,
                price__gte=limit).order_by('price','id')

        completed_amount = 0.0
        remining_amount = order_instance.amount
        orders_to_process = []
        total_price = 0.0

        for sell_order in sell_orders:

            sell_order_amount = sell_order.amount - sell_order.coins_covered

            if remining_amount == sell_order_amount:
                completed_amount += remining_amount
                total_price += sell_order.price * remining_amount
                remining_amount = 0.0
                orders_to_process.append((sell_order, sell_order_amount))
                break
            elif remining_amount > sell_order_amount:
                completed_amount += sell_order_amount
                total_price += sell_order.price  * sell_order_amount
                remining_amount -= sell_order_amount
                orders_to_process.append((sell_order, sell_order_amount))
            else:
                completed_amount += remining_amount
                total_price += sell_order.price * remining_amount
                orders_to_process.append((sell_order, remining_amount))
                remining_amount = 0.0
                break

        transaction_fee = get_transaction_fee(total_price, 'sgd', 'taker')

        sgdwallet, create = SGDWallet.objects.get_or_create(user=order_instance.user)
        current_balance = sgdwallet.amount

        if total_price + float(transaction_fee) + remining_amount * order_instance.price > current_balance:
            return False

        access = create_connection()
        for order_to_process_tuple in orders_to_process:
            access.move(order_to_process_tuple[0].user.username, order_instance.user.username, order_to_process_tuple[1])

            sgdwallet, create = SGDWallet.objects.get_or_create(user=order_instance.user)
            new_balance = sgdwallet.amount + order_to_process_tuple[1] * order_to_process_tuple[0].price
            SGDWallet.objects.filter(user=order_to_process_tuple[0].user).update(amount=new_balance)

            if (order_to_process_tuple[0].amount - order_to_process_tuple[0].coins_covered) ==\
             order_to_process_tuple[1]:
                order_to_process_tuple[0].trade_status = True

            order_to_process_tuple[0].coins_covered += order_to_process_tuple[1]
            order_to_process_tuple[0].save()

        order_instance.coins_covered += completed_amount
        order_instance.trading_fee += float(transaction_fee)
        if remining_amount == 0.0:
            order_instance.trade_status = True
        else:
            order_instance.maker = True

        order_instance.save()

        for order_to_process_tuple in orders_to_process:
            OrderMatchingHistory(processing_order=order_instance, matched_order=order_to_process_tuple[0],
                coins_covered=order_to_process_tuple[1], matching_price=order_to_process_tuple[0].price,
                trading_fee=transaction_fee).save()
            
        balance_amount = current_balance - (total_price + float(transaction_fee) + remining_amount * order_instance.price)
        SGDWallet.objects.filter(user=order_instance.user).update(amount=balance_amount)

        return True

    if order_instance.order_type == '1':
        #processing buy order
        if order_instance.order_mode == '0':
            # market order
            buy_orders = OrderBook.objects.filter(order_type='0', trade_status=False).order_by('-price','id')
        elif order_instance.order_mode == '1':
            #limit order
            buy_orders = OrderBook.objects.filter(order_type='0', trade_status=False, price__gte=order_instance.price).order_by('-price','id')
        elif order_instance.order_mode == '2':
            #stop-limit order
            limit = order_instance.limit
            buy_orders = OrderBook.objects.filter(order_type='0', trade_status=False, price__gte=order_instance.price,
                price__lte=limit).order_by('-price','id')

        completed_amount = 0.0
        remining_amount = order_instance.amount
        orders_to_process = []
        total_price = 0.0

        for buy_order in buy_orders:

            buy_order_amount = buy_order.amount - buy_order.coins_covered

            if remining_amount == buy_order_amount:
                completed_amount += remining_amount
                total_price += buy_order.price * remining_amount
                remining_amount = 0.0
                orders_to_process.append((buy_order, buy_order_amount))
                break
            elif remining_amount > buy_order_amount:
                completed_amount += buy_order_amount
                total_price += buy_order.price  * buy_order_amount
                remining_amount -= buy_order_amount
                orders_to_process.append((buy_order, buy_order_amount))
            else:
                completed_amount += remining_amount
                total_price += buy_order.price * remining_amount
                orders_to_process.append((buy_order, remining_amount))
                remining_amount = 0.0
                break

        transaction_fee = get_transaction_fee(total_price, 'sgd', 'taker')

        sgdwallet, create = SGDWallet.objects.get_or_create(user=order_instance.user)
        current_balance = sgdwallet.amount

        if  float(transaction_fee) > current_balance + total_price:
            return False

        access = create_connection()
        for order_to_process_tuple in orders_to_process:
            access.move(order_instance.user.username, order_to_process_tuple[0].user.username, order_to_process_tuple[1])

            if (order_to_process_tuple[0].amount - order_to_process_tuple[0].coins_covered) ==\
             order_to_process_tuple[1]:
                order_to_process_tuple[0].trade_status = True

            order_to_process_tuple[0].coins_covered += order_to_process_tuple[1]
            order_to_process_tuple[0].save()

        order_instance.coins_covered += completed_amount
        order_instance.trading_fee += float(transaction_fee)
        if remining_amount == 0.0:
            order_instance.trade_status = True
        else:
            order_instance.maker = True

        order_instance.save()

        for order_to_process_tuple in orders_to_process:
            OrderMatchingHistory(processing_order=order_instance, matched_order=order_to_process_tuple[0],
                coins_covered=order_to_process_tuple[1], matching_price=order_to_process_tuple[0].price,
                trading_fee=transaction_fee).save()

        balance_amount = current_balance + total_price
        SGDWallet.objects.filter(user=order_instance.user).update(amount=balance_amount)

        return True


def calculate_24Hr_time():
    current = datetime.datetime.now()
    hr24_before = current - datetime.timedelta(hours=24)
    return current, hr24_before


