from django import forms
from decimal import Decimal

from apps.bitcoin_crypto.models import Transaction, OrderBook, SGDWallet, MinimunCoin
from apps.bitcoin_crypto.utils import get_btc_balance, create_connection, get_balance
from apps.fees.utils import get_transaction_fee
from django.utils.translation import ugettext_lazy as _
from forex_python.bitcoin import BtcConverter
from django.contrib.sites.models import Site


class TransactionForm(forms.Form):
    """
        form validation for bitcoin withdrawal
    """
    address = forms.CharField(max_length=34)
    amount = forms.DecimalField(decimal_places=8)

    def clean_address(self):
        address = self.cleaned_data["address"]
        
        access = create_connection()
        valid = access.validateaddress(str(address))

        if not valid['isvalid']:
             raise forms.ValidationError("Invalid bitcoin address")

        return address

    def clean_amount(self):
        minimum_value = round(Decimal(.00000547), 8)
        amount = self.cleaned_data["amount"]

        balance = get_balance(self.user , 'btc')
        transaction_fee = get_transaction_fee(amount, 'btc', 'withdrawal')
        if amount < minimum_value:
             raise forms.ValidationError("The minimum amount you can withdraw is {}".format(minimum_value))
        elif balance < (amount+transaction_fee):
             raise forms.ValidationError("Amount is greater than wallet balance.")

        return amount

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')

        super().__init__(*args, **kwargs)


class VaultTransactionForm(forms.Form):
    """
        form validation for bitcoin withdrawal
    """
    address = forms.CharField(max_length=34)
    amount = forms.DecimalField(decimal_places=8, min_value=round(Decimal(.00000547),8))

    def clean_address(self):
        address = self.cleaned_data["address"]

        valid = self.access.validateaddress(str(address))

        if not valid['isvalid']:
             raise forms.ValidationError("Invalid bitcoin address")

        return address

    def clean_amount(self):
        amount = self.cleaned_data["amount"]

        account_balance = self.access.getbalance()
        btc_limit_obj, create = MinimunCoin.objects.get_or_create(site=Site.objects.get_current())
        minimum_limit = Decimal(btc_limit_obj.btc_limit)

        if (account_balance - amount) < minimum_limit:
             raise forms.ValidationError("The amount entered will cause the balance to go below minimum limit.")

        return amount

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        self.access = create_connection()

        super().__init__(*args, **kwargs)


class ExchangeForm(forms.ModelForm):
    """
        form to place sell orders and buy orders of different type
    """
    total = forms.FloatField()
    class Meta:
        model = OrderBook
        exclude = ('coins_covered', 'trade_status', 'order_time', 'trading_fee', 'canceled', 'maker')

    def clean(self):
        data = self.cleaned_data
        user = data['user']
        amount = data['amount']
        price = data['price']
        order_type = data['order_type']
        order_mode = data['order_mode']
        
        if amount <= 0:
            raise forms.ValidationError(_("Amount should be greater than zero"))
        if price <= 0:
            raise forms.ValidationError(_("Price should be greater than zero"))

        if order_type == '0':
            try:
                available_sgd_amount = SGDWallet.objects.get(user=user).amount
            except:
                available_sgd_amount = 0.0

            buy_volume = float(price) * float(amount)
            if buy_volume > available_sgd_amount:
                raise forms.ValidationError(_("You don't have sufficient balance in your account."))

            if order_mode == '2':
                stop = data['limit']
                if price < stop:
                    raise forms.ValidationError(_("Stop value can not be greater than price"))

        else:
            available_btc_amount = get_btc_balance(user)
            if available_btc_amount < amount:
                raise forms.ValidationError(_("You don't have sufficient BTC in your wallet."))

            b = BtcConverter()
            sell_volume = float(price) * float(amount)
            available_btc_amount_in_sgd = b.convert_to_btc(sell_volume, 'SGD')
            if available_btc_amount_in_sgd > available_btc_amount:
                raise forms.ValidationError(_("You don't have sufficient balance in your account."))

            if order_mode == '2':
                stop = data['limit']
                if price > stop:
                    raise forms.ValidationError(_("Stop value can not be less than price"))

        return data


class ReportForm(forms.Form):
    fromdate = forms.DateField(input_formats=['%Y-%m-%d'])
    todate = forms.DateField(input_formats=['%Y-%m-%d'])

    def clean(self):
        data = self.cleaned_data
        print(data)
        fromdate = data['fromdate']
        todate = data['todate']

        if fromdate > todate:
            raise forms.ValidationError(_("To date is less than From date"))

        return data


class AddSGDForm(forms.Form):
    """
        validation of sgd amount
    """
    amount = forms.DecimalField(decimal_places=2)

    def clean_amount(self):
        amount = self.cleaned_data["amount"]
        
        if amount <= Decimal(0):
            raise forms.ValidationError("Value should be greater than zero")

        return amount





