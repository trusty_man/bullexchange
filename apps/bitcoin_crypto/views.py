import json
import requests
from decimal import Decimal
from datetime import datetime, timedelta, date
from django.utils import timezone

from django.core import serializers
from django.http import JsonResponse

from django.shortcuts import render, HttpResponse, render_to_response, get_object_or_404, redirect
from django.views.generic import TemplateView, FormView, View, ListView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.conf import settings
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.core.mail import send_mail
from django.utils import six
from django.template.loader import render_to_string
from django.urls import reverse
from django.contrib.sites.models import Site
from django.contrib import messages
from django.db.models import Q

from apps.bitcoin_crypto.forms import ExchangeForm, ReportForm, TransactionForm, VaultTransactionForm, AddSGDForm
from apps.bitcoin_crypto.models import Transaction, Wallet, PendingTransactions, VaultWallet,\
 VaultTransaction, MinimunCoin, WatchOnlyAddress, OrderBook, OrderMatchingHistory, SGDWallet, Notification, NotificationUser
from apps.bitcoin_crypto.utils import *
from apps.authentication.decorators import check_otp, check_2fa
from apps.authentication.models import AccessLog, User, BankAccount
from apps.authentication.forms import BankAccountForm
from django.core.exceptions import PermissionDenied
from django.contrib.auth.mixins import UserPassesTestMixin
from apps.fees.utils import get_transaction_fee
from apps.fees.models import TransactionFee

from apps.authentication.utils import send_otp, _get_pin, send_user_sms, send_admin_sms
from django.contrib.auth.tokens import PasswordResetTokenGenerator
import pyotp

from bitcoinrpc.authproxy import JSONRPCException
from openpyxl import Workbook
from openpyxl.styles import Font, Color, Alignment

from threading import Lock


CURRENCY = {
    '0': 'btc',
    '1': 'eth',
    '2': 'ltc',
    '3': 'xmr',
    '4': 'bch',
    '5': 'btg',

}


class IndexView(TemplateView):
    template_name = 'base.html'


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class WelcomeView(TemplateView):
    template_name = 'index-after-login.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        logs = AccessLog.objects.filter(user=self.request.user)
        if len(logs)>2:
            context["last"] = logs[len(logs)-2]
        context["exchange_form"] = ExchangeForm()
        sell_order = OrderBook.objects.filter(order_type='1', trade_status=False).order_by('price')[:20]
        buy_order = OrderBook.objects.filter(order_type='0', trade_status=False).order_by('-price')[:20]
        open_orders = OrderBook.objects.filter(user=self.request.user, trade_status=False).order_by('-order_time')
        order_history = OrderBook.objects.filter(user=self.request.user, trade_status=True).order_by('-order_time')
        context["sell_order"] = serializers.serialize('json', sell_order)
        context["buy_order"] = serializers.serialize('json', buy_order)
        context["open_orders"] = open_orders
        context["order_history"] = order_history

        # one year price details
        current_date = datetime.datetime.now()
        date_before_one_year = current_date.replace(year=current_date.year-1)
        price_list = []

        date = date_before_one_year.replace(hour=0,minute=0, second=0, microsecond=0)

        while date.date() <= current_date.date():
            next_date = date + timedelta(days=1)
            try:
                closing_order = OrderMatchingHistory.objects.filter(order_matching_time__range=[date, next_date]).latest('order_matching_time')
            except:
                closing_order = None
            
            if closing_order:
                closing_price = closing_order.matching_price
            elif price_list != []:
                closing_price = price_list[-1][1]
            else:
                date = next_date
                continue

            price_list.append([date.timestamp() * 1000, closing_price])
            date = next_date

        context["price_list"] = price_list

        return context


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class SettingsView(TemplateView):
    template_name = 'settings.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['bank_form'] = BankAccountForm()
        context['bank_accounts'] = BankAccount.objects.filter(user=self.request.user)
        return context


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class ExchangeRateView(View):
    """
    Displaying the exchange rate based on the amount.
    If no amount entered will take the minimum transaction amount
    """
    def post(self, request, *args, **kwargs):
        convert_from = CURRENCY[request.POST.get('from')]
        convert_to = CURRENCY[request.POST.get('to')]
        amount = request.POST.get('amount')
        
        if amount:
            params =  {
                    "from": convert_from,
                    "to": convert_to,
                    "amount": amount
                }
            method = "getExchangeAmount"
        else:
            params =  {
                    "from": convert_from,
                    "to": convert_to,
                }
            method = 'getMinAmount'
        data = changelly_transaction(method,params)
        if not data.get('error'):
            request.session['convert_from'] = convert_from
            request.session['convert_to'] = convert_to
            request.session['amount'] = amount
            if not amount:
                request.session['amount'] = '1'
        return HttpResponse(json.dumps(data), content_type='application/json')


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class WalletsView(TemplateView):
    template_name = 'bitcoin/wallets.html'

    def get_context_data(self, *args, **kwargs):
        context = super(WalletsView, self).get_context_data(**kwargs)

        #preventing generating new wallet address when page refresh
        if not Wallet.objects.filter(user=self.request.user).exists():
            create_btc_wallet(self.request.user)

        context['wallets'] = Wallet.objects.filter(user=self.request.user)
        return context


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class TransactionListView(ListView):
    """
    All transactions list of a user
    """
    model = Transaction
    template_name = 'bitcoin/transactions.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        access = create_connection()
        transactions = access.listtransactions(self.request.user.username)
        context['transactions'] = [txn for txn in transactions if txn['category'] == 'receive']
        context['send_list'] = Transaction.objects.filter(user=self.request.user, currency="btc", transaction_type='withdrawal')

        return context


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class SendTransactionView(TemplateView):
    """
        verifing amount and address before sending bitcoin to another address
    """
    template_name = 'bitcoin/send_coin.html'

    def get_context_data(self, *args, **kwargs):
        """
            generating transaction fee list to perform jquery fee amount calculation
        """
        context = super(SendTransactionView, self).get_context_data(**kwargs)
        try:
            fee_obj = TransactionFee.objects.get(currency='btc', fee_type='withdrawal')
            fee_list = fee_obj.transactionfeerange_set.values_list('value','fees')
        except:
            fee_obj = None
            fee_list = {}

        context['fee_dict'] = json.dumps(dict(fee_list))
        context['fee_obj'] = fee_obj
        return context

    def post(self, request, *args, **kwargs):
        """
            verifing submitter address and amount before confirming withdrawal
        """
        form = TransactionForm(request.POST, user=request.user)
        context = self.get_context_data(**kwargs)

        if not request.user.kyc_verified == 'verified' and not request.user.is_superuser :
            context['error'] = "Your account has not been activated yet. please submit kyc details and wait for admin approval"

            return render(request, self.template_name, context)

        if form.is_valid():

            request.session['to'] = form.cleaned_data['address']
            request.session['amount'] = request.POST.get('amount')
            context['form'] = form
            context['success'] = True

            return render(request, self.template_name, context)
        else:
            context['form'] = form

            return render(request,self.template_name, context)


class EmailTokenGenerator(PasswordResetTokenGenerator):
    """ Overriding default Password reset token generator for email confirmation"""
    def _make_hash_value(self, user, timestamp):
        return (six.text_type(user.pk) + six.text_type(timestamp)) +  six.text_type(user.is_active)

email_token = EmailTokenGenerator()


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class SendConfirmView(View):
    def post(self, request, *args, **kwargs):
        
        sms_redirect = False

        if request.POST.get('to') and request.POST.get('amount'):
            request.session['address'] = request.POST.get('to')
            request.session['amount'] = request.POST.get('amount')

            try:
                del request.session['withdraw-otp-verified']
            except:
                pass

        if request.user.sms_otp:

            otp_verified = request.session.get('withdraw-otp-verified', False)
            
            if not otp_verified and  self.request.POST.get('value') and not request.POST.get('resend-otp'):
                if self.request.POST.get('value') == self.request.session['withdraw-otp']:
                    request.session['withdraw-otp-verified'] = True
                    sms_redirect = True
                else:
                    return HttpResponse(json.dumps({"status":"otp-not-verified"}), content_type='application/json')

            elif not otp_verified:
                pin =  _get_pin(self)
                self.request.session['withdraw-otp'] = pin
                send_otp(self, pin, self.request.user.phone_number)
                return HttpResponse(json.dumps({"status":"get-otp"}), content_type='application/json')

        if request.user.google_2fa:

            if self.request.POST.get('value') and not sms_redirect:
                totp_code = self.request.POST.get('value', None)
                totp = pyotp.TOTP(self.request.user.google_2fa_key)

                if not totp.verify(totp_code):
                    return HttpResponse(json.dumps({"status":"totp-not-verified"}), content_type='application/json')
            else:
                return HttpResponse(json.dumps({"status":"get-totp"}), content_type='application/json')

        transaction_fee = get_transaction_fee(request.session['amount'], 'btc', 'withdrawal')
        pending_transaction = PendingTransactions.objects.create(user=request.user, amount=request.session['amount'],
            currency='BTC', transaction_to=request.session['address'], transaction_fee=str(transaction_fee))

        token = email_token.make_token(request.user)
        uidb64 = urlsafe_base64_encode(force_bytes(pending_transaction.pk)).decode("utf-8")

        html_message = render_to_string('bitcoin/confirm-withdrawal-email.html', {
            'pending_transaction': pending_transaction,
            'uri': reverse('coins:confirm_withdrawal', kwargs={'uidb64':uidb64, 'token': token}),
            'domain': self.request.scheme+"://"+"bullxchange.io",
        })

        try:
            del request.session['withdraw-otp-verified']
        except:
            pass

        try:
            send_mail('Confirm Withdrawal',
                '',
                settings.DEFAULT_FROM_EMAIL,
                [request.user.email],
                html_message = html_message,
                fail_silently=False
            )
            return HttpResponse(json.dumps({"success":True}), content_type='application/json')
        except:
            return HttpResponse(json.dumps({"error":"something went wrong"}), content_type='application/json')


class SendEmailConfirmView(View):

    def get(self, request, *args, **kwargs):
        pk = force_text(urlsafe_base64_decode( kwargs.get('uidb64')))
        token = kwargs.get('token')

        try:
            pending_transaction = get_object_or_404(PendingTransactions, pk=pk)
        except:
            context={'error' : 'Transaction already completed or not a valid link'}

            return render(request,'bitcoin/transaction_confirm.html', context)

        context = {'success': False}

        if email_token.check_token(pending_transaction.user, token):

            balance = get_balance(pending_transaction.user, 'btc')
            access = create_connection()
            address = pending_transaction.transaction_to
            amount = Decimal(pending_transaction.amount)
            transaction_fee = Decimal(pending_transaction.transaction_fee)

            if balance < (amount+transaction_fee):
                context = {'message': 'Insufficient Balance.'}
                return render(request,'bitcoin/transaction_confirm.html', context)

            btc_limit_obj, create = MinimunCoin.objects.get_or_create(site=Site.objects.get_current())
            minimum_limit = Decimal(btc_limit_obj.btc_limit)

            balance_status = get_account_balance() - (amount+transaction_fee)
            if (get_account_balance() < (amount+transaction_fee)) or (balance_status < minimum_limit):
                balance = balance - (amount+transaction_fee)
                
                if (get_account_balance() < (amount+transaction_fee)):
                    message = "User requesting more amount than existing balance. Please Visit Bullexchange Wallet Update page to view the Amount need to maintain mininum Balance."
                    email_title = 'Insufficient Fund in Hot Wallet'
                    context = {'success': True}
                else:
                    message = 'Minimum Balance limit Exceed in Bullexchange. Please Update wallet Amount'
                    email_title = 'Minimum Balance limit Exceed'

                valid = access.validateaddress(address)

                if valid['isvalid']:
                    Transaction.objects.create(user=pending_transaction.user, currency="btc", balance=balance, transaction_type='withdrawal',
                   amount=amount, transaction_fee=str(transaction_fee), transaction_id='', transaction_to=address, pending=True)
                    context = {'success': True}
                else:
                    context = {'message': 'Transaction Failed. Invalid Address'}
                            
                pending_transaction.delete()

                if btc_limit_obj.low_limit_alert:

                    users = User.objects.filter(is_superuser=True).exclude(phone_number="", email="")

                    for admin_user in users:

                        send_mail(email_title,
                            message,
                            settings.DEFAULT_FROM_EMAIL,
                            [admin_user.email],
                            fail_silently=True
                        )

                        send_admin_sms(admin_user.phone_number, message)

                    btc_limit_obj.low_limit_alert = False
                    btc_limit_obj.save()

                return render(request,'bitcoin/transaction_confirm.html', context)

            valid = access.validateaddress(address)

            if valid['isvalid']:
                balance = balance - (amount+transaction_fee)
                Transaction.objects.create(user=pending_transaction.user, currency="btc", balance=balance, transaction_type='withdrawal', 
                   amount=amount, transaction_fee=str(transaction_fee), transaction_id='', transaction_to=address, pending=True)
                pending_transaction.delete()
                context = {'success': True}

        return render(request,'bitcoin/transaction_confirm.html', context)


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class PendingTransactionListView(UserPassesTestMixin, View):

    def test_func(self):
        if not self.request.user.is_superuser:
            raise PermissionDenied()
        return True

    def get(self, request, *args, **kwargs):
        pending_transactions = Transaction.objects.filter(pending=True)
        context = {
            'pending_transactions': pending_transactions
        }
        return render(request,'bitcoin/pending_transactions.html', context)


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class ConfirmPendingTransactionView(UserPassesTestMixin, View):

    def test_func(self):
        if not self.request.user.is_superuser:
            raise PermissionDenied()
        return True

    def get(self, request, *args, **kwargs):

        try:
            transaction = get_object_or_404(Transaction, pk=self.kwargs['pk'], pending=True)
        except:
            error = 'Transaction object not found'

        try:
            status = complete_pending_transaction(transaction)
            
            if status == True:
                notification = Notification.objects.create(
                    notification='Withdrawal amount %s BTC to address %s has approved by admin'
                    %(transaction.amount, transaction.transaction_to))
                NotificationUser.objects.create(notification=notification, user=transaction.user)

        except JSONRPCException as e:
            status = False
            error = e.error['message']


        if status == True:
            return HttpResponse(json.dumps({"success": True}), content_type='application/json')

        return HttpResponse(json.dumps({"error": error}), content_type='application/json')


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class RejectPendingTransactionView(UserPassesTestMixin, View):
    template_name = 'bitcoin/reject_trnasaction.html'

    def test_func(self):
        if not self.request.user.is_superuser:
            raise PermissionDenied()
        return True

    def get(self, request, *args, **kwargs):
        transaction = get_object_or_404(Transaction, pk=self.kwargs['pk'], pending=True)
        context ={
            'transaction': transaction
        }           
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):

        feedback = request.POST.get('feedback', False)
        transaction = get_object_or_404(Transaction, pk=self.kwargs['pk'], pending=True)

        if not feedback:
            context ={
                'transaction': transaction,
                'feedback_error': 'This field is required'
            }  
            return render(request, self.template_name, context)

        transaction.rejected = True
        transaction.pending = False
        transaction.save()

        notification = Notification.objects.create(
            notification='Withdrawal amount %s BTC to address %s has rejected by admin<br>%s'
            %(transaction.amount, transaction.transaction_to, feedback))
        NotificationUser.objects.create(notification=notification, user=transaction.user)

        context ={
            'transaction': transaction,
            'success': True
        }         
        return render(request, self.template_name, context)


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class GetAllUserAddresse(TemplateView):
    template_name = 'bitcoin/user_accounts.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        wallet_object = Wallet.objects.all()
        context['wallet_obj'] = wallet_object
        
        return render(request,self.template_name, context)


class TransactionRecordView(TemplateView):

    def get(self, request, *args, **kwargs):
        access = create_connection()
        withdraw_transactions = Transaction.objects.all().exclude(transaction_type='fee_withdrawal')

        filtered_deposit = []
        for user in User.objects.all():
            deposit_transaction = access.listtransactions(user.username)
            for trnas in deposit_transaction:
                if trnas['category'] == 'receive':
                    filtered_deposit.append(trnas)


        return self.render_to_response({
            'withdraw_transactions':withdraw_transactions,
            'deposit_transaction':filtered_deposit
        })


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class SetMinimunLimitBTC(UserPassesTestMixin, TemplateView):
    template_name = 'bitcoin/set_minimum_limit.html'

    def test_func(self):
        if not self.request.user.is_superuser:
            raise PermissionDenied()
        return True

    def post(self, request, *args, **kwargs):
        value = request.POST.get('minimum_limit')

        try:
            decimal_value = Decimal(value)
        except:
            return HttpResponse(json.dumps({"error": "Invalid Amount."}), content_type='application/json')

        min_value_obj, create = MinimunCoin.objects.get_or_create(site=Site.objects.get_current())
        min_value_obj.btc_limit = value
        min_value_obj.save()
        return HttpResponse(json.dumps({"success": True}), content_type='application/json')


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class WalletToVault(UserPassesTestMixin, TemplateView):
    template_name = 'bitcoin/send_to_vault.html'

    def test_func(self):
        if not self.request.user.is_superuser:
            raise PermissionDenied()
        return True

    def post(self, request, *args, **kwargs):

        context = self.get_context_data(**kwargs)

        if not request.user.email:
            context['error'] = "For Vault transaction, User need to Add a Valid Email Address."
            return render(request, self.template_name, context)

        # if not request.user.sms_otp and not request.user.google_2fa:
        #     context['error'] = "For Vault transaction, User need to Enable google authentication and sms otp verification."
        #     return render(request, self.template_name, context)

        form = VaultTransactionForm(request.POST, user=request.user)

        if form.is_valid():

            request.session['to'] = form.cleaned_data['address']
            request.session['amount'] = request.POST.get('amount')

            # try:
            #     del request.session['withdraw-otp-verified']
            # except:
            #     pass

            # pin =  _get_pin(self)
            # self.request.session['withdraw-otp'] = pin
            # send_otp(self, pin, self.request.user.phone_number)

            # return redirect(reverse('coins:confirm_wallet_to_vault'))

            """
                code only for testing starts
            """
            amount = request.session['amount']
            address = request.session['to']

            pending_transaction = PendingTransactions.objects.create(user=request.user, amount=amount,
            currency='BTC', transaction_to=address)
            token = email_token.make_token(request.user)
            uidb64 = urlsafe_base64_encode(force_bytes(pending_transaction.pk)).decode("utf-8")

            html_message = render_to_string('bitcoin/confirm-withdrawal-email.html', {
                'pending_transaction': pending_transaction,
                'domain': self.request.scheme+"://"+"bullxchange.io",
                'uri': reverse('coins:vault_confirm', kwargs={'uidb64':uidb64, 'token': token}),
            })

            try:
                send_mail('Confirm Withdrawal',
                    '',
                    settings.DEFAULT_FROM_EMAIL,
                    [request.user.email],
                    html_message = html_message,
                    fail_silently=False
                )
            except:
                pass
                
            context['success'] = True
            """
                code only for testing ends
            """

        context['form'] = form
        return render(request, self.template_name, context)


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class ConfirmWalletToVault(View):
    template_name = 'bitcoin/send_to_vault.html'

    def get(self, request, *args, **kwargs):

        form = VaultTransactionForm(initial={'address': request.session['to'],
            'amount': request.session['amount']}, user=request.user)

        context ={
        'sms_verification': True,
        'form': form
        }

        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):

        sms_redirect = False
        otp_verified = request.session.get('withdraw-otp-verified', False)
        
        if not otp_verified and  self.request.POST.get('value') and not request.POST.get('resend-otp'):
            if self.request.POST.get('value') == self.request.session['withdraw-otp']:
                request.session['withdraw-otp-verified'] = True
                sms_redirect = True
            else:
                return HttpResponse(json.dumps({"status":"otp-not-verified"}), content_type='application/json')

        elif not otp_verified:
            pin =  _get_pin(self)
            self.request.session['withdraw-otp'] = pin
            send_otp(self, pin, self.request.user.phone_number)
            return HttpResponse(json.dumps({"status":"get-otp"}), content_type='application/json')


        if self.request.POST.get('value') and not sms_redirect:
            totp_code = self.request.POST.get('value', None)
            totp = pyotp.TOTP(self.request.user.google_2fa_key)

            if not totp.verify(totp_code):
                return HttpResponse(json.dumps({"status":"totp-not-verified"}), content_type='application/json')
        else:
            return HttpResponse(json.dumps({"status":"get-totp"}), content_type='application/json')


        amount = request.session['amount']
        address = request.session['to']

        pending_transaction = PendingTransactions.objects.create(user=request.user, amount=amount,
        currency='BTC', transaction_to=address)
        token = email_token.make_token(request.user)
        uidb64 = urlsafe_base64_encode(force_bytes(pending_transaction.pk)).decode("utf-8")

        html_message = render_to_string('bitcoin/confirm-withdrawal-email.html', {
            'pending_transaction': pending_transaction,
            'domain': self.request.scheme+"://"+"bullxchange.io",
            'uri': reverse('coins:vault_confirm', kwargs={'uidb64':uidb64, 'token': token}),
        })

        try:
            send_mail('Confirm Withdrawal',
                '',
                settings.DEFAULT_FROM_EMAIL,
                [request.user.email],
                html_message = html_message,
                fail_silently=False
            )

            return HttpResponse(json.dumps({"success": True}), content_type='application/json')
        except:
            error = "Email can not send please check your email address and try again"
            return HttpResponse(json.dumps({"error": error}), content_type='application/json')


class WalletToVaultEmailConfirm(View):

    def get(self, request, *args, **kwargs):
        pk = force_text(urlsafe_base64_decode( kwargs.get('uidb64')))
        token = kwargs.get('token')
        try:
            pending_transaction = get_object_or_404(PendingTransactions, pk=pk)
        except:
            context = {'error' : 'Vault transaction already completed or not a valid link' }
            return render(request,'bitcoin/vault_transaction_confirm.html', context)
            
        context = {'success': False}

        if email_token.check_token(pending_transaction.user, token):

            balance = get_account_balance()
            access = create_connection()
            address = pending_transaction.transaction_to
            amount = Decimal(pending_transaction.amount)

            btc_limit_obj, create = MinimunCoin.objects.get_or_create(site=Site.objects.get_current())
            minimum_limit = Decimal(btc_limit_obj.btc_limit)
            if (balance - amount) < minimum_limit:
                context = {
                    'message' : 'Cancelled!. Transaction will not maintain Minimun Balance Limit.'
                }
                return render(request,'bitcoin/vault_transaction_confirm.html', context)

            if balance >= amount:

                try:
                    access.importaddress(address,'',False)
                except:
                    context = {
                        'message' : 'Canceled!. The wallet already contains the private key for this address'
                    }
                    return render(request,'bitcoin/vault_transaction_confirm.html', context)

                valid = access.sendtoaddress(address, amount)

                if valid:
                    WatchOnlyAddress.objects.get_or_create(address=address)
                    txn_fee = access.gettransaction(valid)['fee']
                    balance = balance - amount
                    VaultTransaction.objects.create(user=pending_transaction.user, currency="btc", balance=balance, 
                        amount=amount, mining_fee=str(abs(txn_fee)), transaction_id=valid, transaction_to=address)
                    pending_transaction.delete()
                    context = {'success': True}

        return render(request,'bitcoin/vault_transaction_confirm.html', context)


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class VaultTransactionListView(UserPassesTestMixin, View):

    template_name = 'bitcoin/vault_transactions.html'

    def test_func(self):
        if not self.request.user.is_superuser:
            raise PermissionDenied()
        return True

    def get(self, request, *args, **kwargs):
        access = create_connection()
        withdraw_transactions = VaultTransaction.objects.filter(transaction_type='to_vault').order_by('-date')
        deposit_transaction =  VaultTransaction.objects.filter(transaction_type='from_vault').order_by('-date')
        context = {
            'withdraw_transactions':withdraw_transactions,
            'deposit_transaction':deposit_transaction
        }
        return render(request, self.template_name, context)




@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class ListVaultView(UserPassesTestMixin, View):

    def test_func(self):
        if not self.request.user.is_superuser:
            raise PermissionDenied()
        return True

    def get(self, request, *args, **kwargs):

        checkwatchonlyaddress()
        vault_list = WatchOnlyAddress.objects.all()
        
        context = {
            'vault_list': vault_list
        }
        return render(request, 'bitcoin/vault_list.html', context)


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class VaultToWalletView(UserPassesTestMixin, TemplateView):
    """send coin back to requested users"""
    template_name = 'bitcoin/vault_to_wallet.html'

    def test_func(self):
        if not self.request.user.is_superuser:
            raise PermissionDenied()
        return True

    def post(self, request, *args, **kwargs):
        vault_address = request.POST.get('vault_address')

        if not request.user.email:
            return HttpResponse(json.dumps({"error": "For Vault transaction, User need to Add a Valid Email Address."}), content_type='application/json')

        # if not request.user.sms_otp and not request.user.google_2fa:
        #     return HttpResponse(json.dumps({"error": "For Vault transaction, User need to Enable google authentication and sms otp verification."}), content_type='application/json')


        sms_redirect = False
        if vault_address:

            request.session['address'] = vault_address

            try:
                del request.session['vault2wallet-otp-verified']
            except:
                pass

        # if request.user.sms_otp:
        #     print('sms otp')
        #     otp_verified = request.session.get('vault2wallet-otp-verified', False)
            
        #     if not otp_verified and  self.request.POST.get('value') and not request.POST.get('resend-otp'):
        #         print(self.request.session['vault2wallet-otp'])
        #         print(self.request.POST.get('value'))
        #         if self.request.POST.get('value') == self.request.session['vault2wallet-otp']:
        #             print('sms otp verified')
        #             request.session['vault2wallet-otp-verified'] = True
        #             sms_redirect = True
        #         else:
        #             print('sms otp invalid')
        #             return HttpResponse(json.dumps({"status":"otp-not-verified"}), content_type='application/json')

        #     elif not otp_verified:
        #         print('sms otp not verified')
        #         pin =  _get_pin(self)
        #         self.request.session['vault2wallet-otp'] = pin
        #         send_otp(self, pin, self.request.user.phone_number)
        #         return HttpResponse(json.dumps({"status":"get-otp"}), content_type='application/json')

        # if request.user.google_2fa:
        #     print('totp')
        #     if self.request.POST.get('value') and not sms_redirect:
        #         totp_code = self.request.POST.get('value', None)
        #         totp = pyotp.TOTP(self.request.user.google_2fa_key)

        #         if not totp.verify(totp_code):
        #             print('totp not verified')
        #             return HttpResponse(json.dumps({"status":"totp-not-verified"}), content_type='application/json')
        #     else:
        #         print('totp get')
        #         return HttpResponse(json.dumps({"status":"get-totp"}), content_type='application/json')
        #     print('totp verified')

        try:
            del request.session['vault2wallet-otp-verified']
        except:
            pass

        vault_address = request.session['address']

        access = create_connection()

        pre_balance = get_account_balance()

        try:
            result = access.importprivkey(vault_address,'', False)
        except:
            return HttpResponse(json.dumps({"error": "Invalid private key<br><small>The private key should be encoded in base58check using wallet import format (WIF)<small>"}), content_type='application/json')

        new_balance = balance = get_account_balance()
        amount_added = new_balance - pre_balance

        # complete_pending_transactions()

        public_key = get_publict_key(vault_address)

        VaultTransaction.objects.create(user=request.user, currency="btc", balance='', 
                        amount= amount_added, transaction_id='', transaction_to='',
                        transaction_type='from_vault', transaction_from=public_key)

        btc_limit_obj, create = MinimunCoin.objects.get_or_create(site=Site.objects.get_current())
        btc_limit_obj.low_limit_alert = True
        btc_limit_obj.save()

        return HttpResponse(json.dumps({"success": True}), content_type='application/json')


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class OrderView(View):

    def post(self, request, *args, **kwargs):

        if request.user.kyc_verified == 'not-verified':
            return JsonResponse({'status': False, 'error': 'Account not activated, Please verify kyc details'})

        if request.user.kyc_verified == 'processing':
            return JsonResponse({'status': False, 'error': 'Account not activated, Please wait for kyc approval'})

        if request.user.kyc_verified == 'rejected':
            return JsonResponse({'status': False, 'error': 'Kyc details rejected, Please contact administrator for details'})

        form = ExchangeForm(request.POST)

        if form.is_valid():

            try:
                # sheduling mutiple request access (but only works for single server process)
                lock = Lock()
                lock.acquire()

                # processing and saving order
                form_object = form.save(commit=False)
                status = complete_order(request, form_object)

                if status == False:
                    return JsonResponse({'status': False, 'error': 'You don\'t have sufficient balance in your account to pay trading fees.'})
                
            finally:
                lock.release()

            form_object.save()
                        
            buy_order = OrderBook.objects.filter(order_type='0', trade_status=False).order_by('-price')[:20]
            sell_order = OrderBook.objects.filter(order_type='1', trade_status=False).order_by('price')[:20]

            if request.POST.get('order_type') == '0':
                if form_object.trade_status == True:
                    message = 'Buy order completed Successfully'
                else:
                    message = 'Buy order placed'
                    
                data = serializers.serialize('json', buy_order)
            else:
                if form_object.trade_status == True:
                    message = 'Sell order completed Successfully'
                else:
                    message = 'Sell order placed'

                data = serializers.serialize('json', sell_order)
            return JsonResponse({'status':True,
                                 'data':data,
                                 'message':message
                                 }, safe=False)
        else:
            errors = form.errors
            if request.POST.get('order_type') == '0':
                return JsonResponse({'status':False, 'error': errors['__all__'][0]})
            else:
                return JsonResponse({'status':False, 'error': errors['__all__'][0]})


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class AjaxDepthChartView(View):

    def get(self, request, *args, **kwargs):
        buy_order = OrderBook.objects.filter(order_type='0', trade_status=False).order_by('-price')
        sell_order = OrderBook.objects.filter(order_type='1',  trade_status=False).order_by('price')
        return HttpResponse(json.dumps({'bids': self.list_data(buy_order), 'asks': self.list_data(sell_order)}))

    def list_data(self, object):
        list_formatted_data = [[data_obj.price, data_obj.amount] for data_obj in object]
        return list_formatted_data


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class AjaxCalculateBalance(View):

    def get(self, request, *args, **kwargs):
        order_type = request.GET.get('type')
        price = request.GET.get('type')
        amount = request.GET.get('type')


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class BuyOrderList(ListView):
    """
        listing all buy orders placed by current user
    """
    context_object_name = 'buy_order_list'
    template_name = 'bitcoin/buy_order_list.html'

    def get_queryset(self, *args, **kwargs):

        buy_orders = OrderBook.objects.filter(order_type=0,user=self.request.user).order_by('-order_time')
        return buy_orders


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class SellOrderList(ListView):
    """
        listing all sell orders placed by current user
    """
    context_object_name = 'sell_order_list'
    template_name = 'bitcoin/sell_order_list.html'

    def get_queryset(self, *args, **kwargs):

        sell_orders = OrderBook.objects.filter(order_type=1,user=self.request.user).order_by('-order_time')
        return sell_orders


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class DeleteOrderView(View):
    """
        delete already placed buy or sell order
    """
    def get(self, request, *args, **kwargs):

        order_object = get_object_or_404(OrderBook, pk=kwargs['pk'], user=request.user, trade_status= False)
        sgd_wallet_obj, create = SGDWallet.objects.get_or_create(user=request.user)

        if order_object.order_type == '0':
            new_balance = (order_object.amount - order_object.coins_covered) * order_object.price  + sgd_wallet_obj.amount
        
        if order_object:
            if order_object.coins_covered == 0.0:
                delted = order_object.delete()
                print(delted)
            else:
                OrderBook.objects.filter(id=order_object.pk).update(trade_status=True, canceled=True)
            
            if order_object.order_type == '0':
                sgd_wallet_obj.amount = new_balance
            sgd_wallet_obj.save()

            return JsonResponse({'success': True})

        return JsonResponse({'success': False})



@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class NotificationListView(View):
    """
        Listing all notifications of current user
    """
    def get(self, request, *args, **kwargs):
        
        notification_count = NotificationUser.objects.filter(user=self.request.user, is_readed=False).count()

        NotificationUser.objects.filter(user=self.request.user).update(is_readed=True)

        context = {
            'notifications': Notification.objects.filter(user= self.request.user).order_by('-date'),
            'notification_count': notification_count
        }

        return render(request, 'bitcoin/notifications.html', context)


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class DownloadReport(View):
    """
        retrive deposit and withdrawal of a user in xlsx format
    """
    def get(self, request, *args, **kwargs):
        # rendering ReportForm in template
        context ={
            'form': ReportForm()
        }
        return render(request, 'bitcoin/report.html', context)

    def post(self, request, *args, **kwargs):
        # filtering Transaction object using date rage and generating xlsx sheet
        form = ReportForm(request.POST)

        #form validation
        if form.is_valid():
            start_date = form.cleaned_data['fromdate']
            end_date = form.cleaned_data['todate']+ timedelta(days=1)
        else:
            context ={
                'form': form
            }
            return render(request, 'bitcoin/report.html', context)

        # queryset transactions in date rage
        transactions = Transaction.objects.filter(user=request.user, pending=False, transaction_type__in=['withdrawal','deposit'], 
            date__range=[start_date,end_date]).order_by('date')

        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = 'attachment; filename="somefilename.xlsx"'

        wb = Workbook()
        ws = wb.active

        row_num = 0

        columns = [
            (u"Date", 15),
            (u"Type", 70),
            (u"Currency", 70),
            (u"Amount", 70),
            (u"Transaction Fee",70)
        ]
        ws.column_dimensions["A"].width = 20
        ws.column_dimensions["B"].width = 12
        ws.column_dimensions["C"].width = 12
        ws.column_dimensions["D"].width = 20
        ws.column_dimensions["E"].width = 20

        for col_num in range(len(columns)):
            c = ws.cell(row=row_num + 1, column=col_num + 1)
            c.value = columns[col_num][0]
            c.font = Font(bold=True)

        for obj in transactions:
            row_num += 1
            row = [
                obj.date.strftime('%d/%m/%y %H:%M:%S'),
                obj.transaction_type.capitalize(),
                obj.currency.upper(),
                obj.amount,
                obj.transaction_fee
            ]
            for col_num in range(len(row)):
                c = ws.cell(row=row_num + 1, column=col_num + 1)
                c.value = row[col_num]
                c.alignment = Alignment(wrapText=True)

        wb.save(response)

        return response


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class SGDWalletsView(ListView):
    """
        list all user sgd wallets
    """
    context_object_name = 'sgd_wallet_list'
    template_name = 'bitcoin/sgd-wallet-list.html'

    def get_queryset(self, *args, **kwargs):

        sgd_wallet_list = SGDWallet.objects.all()

        q = self.request.GET.get('q', None)

        if q:
            sgd_wallet_list = sgd_wallet_list.filter(Q(user__username__icontains=q))

        return sgd_wallet_list


@method_decorator(login_required, name='dispatch')
@method_decorator(check_otp, name='dispatch')
@method_decorator(check_2fa, name='dispatch')
class AddSGDView(View):
    """
        updating user wallet amount
    """
    template_name ='bitcoin/add_sgd.html'
    form_class = AddSGDForm

    def get(self, request, *args, **kwargs):
        wallet = SGDWallet.objects.get(pk=kwargs['pk'])
        form = AddSGDForm()

        context ={
            'wallet': wallet,
            'form': form
        }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        wallet = SGDWallet.objects.get(pk=kwargs['pk'])
        form = AddSGDForm(request.POST)

        if form.is_valid():
            amount = float(form.cleaned_data['amount'])
            wallet.amount += amount
            wallet.save()

            messages.success(request, 'SGD Wallet updated of user %s' %wallet.user.username)
            return redirect(reverse('coins:sgd_wallets'))
        
        context = {
            'form': form,
            'wallet': wallet
        }
        return render(request, self.template_name, context)


class AjaxOrderBookView(View):
    def get(self, request, *args, **kwargs):
        buy_order = OrderBook.objects.filter(order_type='0', trade_status=False).order_by('-price')[:20]
        sell_order = OrderBook.objects.filter(order_type='1', trade_status=False).order_by('price')[:20]
        json_buy_order = serializers.serialize('json', buy_order)
        json_sell_order = serializers.serialize('json', sell_order)
        return JsonResponse({
            'json_buy_order': json_buy_order,
            'json_sell_order': json_sell_order,
        }, safe=False)