from django.contrib import admin
from . models import Wallet, MinimunCoin, ExchangeTaxRates, OrderBook, SGDWallet, Notification

# Register your models here.

admin.site.register(Wallet)
admin.site.register(MinimunCoin)
admin.site.register(ExchangeTaxRates)
admin.site.register(OrderBook)
admin.site.register(SGDWallet)
